#include "MyRectangle.h"
#define CIMGGIP_MAIN
#include "CImgGIP05Mock.h"

MyRectangle::MyRectangle(int x1, int y1, int x2, int y2) {
    this->x1 = x1;
    this->y1 = y1;
    this->x2 = x2;
    this->y2 = y2;
}

MyRectangle::MyRectangle() {
    // Default parameters
    this->x1 = 0;
    this->y1 = 0;

    this->x2 = 20;
    this->y2 = 20;
}

int MyRectangle::get_x1() {
    return this->x1;
}

int MyRectangle::get_x2() {
    return this->x2;
}

int MyRectangle::get_y1() {
    return this->y1;
}

int MyRectangle::get_y2() {
    return this->y2;
}

void MyRectangle::set_x1(int x) {
    this->x1 = x;
}

void MyRectangle::set_x2(int x) {
    this->x2 = x;
}

void MyRectangle::set_y1(int y) {
    this->y1 = y;
}

void MyRectangle::set_y2(int y) {
    this->y2 = y;
}

void MyRectangle::draw() {
    gip_draw_rectangle(this->x1, this->y1, this->x2, this->y2, blue);
}

bool MyRectangle::does_not_collide_with(const MyRectangle& other) const {
    // std::cout << "(" << this->x1 << "|" << this->y1 << "), ";
    // std::cout << "(" << this->x2 << "|" << this->y2 << ")\n";

    // std::cout << "(" << other.x1 << "|" << other.y1 << "), ";
    // std::cout << "(" << other.x2 << "|" << other.y2 << ")\n\n\n";
    // // this upper edge is under other lower edge
    if (this->y1 > other.y2)
        return true;

    // this lower edge is above other upper edge
    if (this->y2 < other.y1)
        return true;

    // this left edge is the the right of other right edge
    if (this->x1 > other.x2)
        return true;

    // this right edge is to the left of left edge
    if (this->x2 < other.x1)
        return true;

    return false;        
}