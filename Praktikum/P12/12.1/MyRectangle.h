#include "CImgGIP05.h"
#include <iostream>

class MyRectangle {
private:
    int x1, y1, x2, y2;

public:
    int get_x1();
    void set_x1(int x);

    int get_x2();
    void set_x2(int x);

    int get_y1();
    void set_y1(int y);

    int get_y2();
    void set_y2(int y);

    void draw();

    bool does_not_collide_with(const MyRectangle& other) const;

    MyRectangle(int x1, int y1, int x2, int y2);

    MyRectangle();
};