#include <iostream>

using namespace std;

struct TListenKnoten {
    int data;
    TListenKnoten * next;
    TListenKnoten * prev;
};


/**
 * Search for the value vor_wert then add wert_neu in front of it. If not found, add
 * wert_neu to the end.
 *
 * @param anker first elem of the list
 * @param wert_neu the value to add to the list
 * @param vor_wert the value to add the new value behind
 */
void in_liste_einfuegen(TListenKnoten* &anker, int wert_neu, int vor_wert) {

    if (anker == nullptr) { //if the list is empty, add the new node as the head
        anker = new TListenKnoten;
        anker->data = wert_neu;
        anker->prev = nullptr;
        anker->next = nullptr;
        return;
    }

    TListenKnoten * node;

    // find the value or find the end of the list, whichever comes first
    for (node = anker; ((node->data != vor_wert) &&
                       (node->next != nullptr)); node = node->next);

    TListenKnoten *new_node = new TListenKnoten;
    new_node->data = wert_neu;


    // if value found, insert a new node before it
    if (node->data == vor_wert) {

        // if is the first node, change the anchor
        if (node == anker) {
            new_node->next = anker;
            new_node->next->prev = new_node;
            new_node->prev = nullptr;
            anker = new_node;

            return;
        }

        // else just insert the new node before it
        new_node->next = node;
        new_node->prev = node->prev;
        node->prev = new_node;
        new_node->prev->next = new_node;
        return;
    }

    // if reached the end and vor_wert not found, insert a new node at the end
    if (node->next == nullptr) {
        new_node->prev = node;
        new_node->next = nullptr;
        node->next = new_node;
    }
}


/**
 * Remove the first elem that has the value wert from the list
 * @param anker first elem of the list
 * @param wert the value to look for
 */
void aus_liste_loeschen(TListenKnoten* &anker, int wert) {
    // browse the list
    for (TListenKnoten * node = anker; node != nullptr; node = node->next) {
        if (node->data == wert) { // if found this node
            // detach it from the list

            if (node != anker) { // if not the first elem
                node->prev->next = node->next;
                node->next->prev = node->prev;
            } else { // else if the node to remove is the first elem
                if (anker->next != nullptr) { // if list contains more than 1 item
                    // change the anchor
                    anker = anker->next;
                    anker->prev = nullptr;
                } else { // if list contains only 1 item
                    // change the anchor
                    anker = nullptr;
                }
            }
            break;
        }
    }
}


/**
 * Remove the entire list
 * @param anker the first elem
 */
void liste_loeschen(TListenKnoten* &anker) {
    anker = nullptr;
}


/**
 * Print the list backwards
 * @param anker the first elem
 */
void liste_ausgeben_rueckwaerts(TListenKnoten* anker) {
    if (anker == nullptr)
        cout << "Leere Liste\n";
    else {
        // find last node
        TListenKnoten * last;
        for (last = anker; last->next != nullptr; last = last->next);

        // print the list
        cout << "[ ";
        TListenKnoten *node;

        for (node = last; node != anker; node = node->prev)
            cout << node->data << " , ";

        cout << node->data << " ]\n";
    }
}


/**
 * Add a new elem to the end of the list
 * @param anker the first elem
 * @param wert the value to add to
 */
void hinten_anfuegen(TListenKnoten * & anker, const int wert) {
    TListenKnoten * neuer_eintrag = new TListenKnoten;
    neuer_eintrag -> data = wert;
    neuer_eintrag -> next = nullptr;

    // add the first elem
    if (anker == nullptr)
        anker = neuer_eintrag;
    else {
        TListenKnoten * ptr = anker;
        while (ptr -> next != nullptr)
            ptr = ptr -> next;
        ptr -> next = neuer_eintrag;

        // attach prev back to the previous elem
        neuer_eintrag->prev = ptr;
    }
}


/**
 * Convert the list to a string
 * @param anker the first elem
 * @return a string represents the list
 */
string liste_als_string(TListenKnoten * anker) {
    string resultat = "";
    if (anker == nullptr)
        return "Leere Liste";
    else {
        resultat += "[ ";
        TListenKnoten * ptr = anker;
        do {
            resultat += std::to_string(ptr -> data);
            if (ptr -> next != nullptr) resultat += " , ";
            else resultat += " ";
            ptr = ptr -> next;
        } while (ptr != nullptr);
        resultat += "]";
    }
    return resultat;
}


/**
 * Print out the list
 * @param anker the first elem
 */
void liste_ausgeben(TListenKnoten * anker) {
    cout << liste_als_string(anker) << endl;
}


int main() {
    const int laenge = 10;
    TListenKnoten * anker = nullptr;
    liste_ausgeben(anker);
    liste_ausgeben_rueckwaerts(anker);
    liste_loeschen(anker);

    hinten_anfuegen(anker, 77);
    hinten_anfuegen(anker, 88);
    hinten_anfuegen(anker, 99);

    liste_ausgeben(anker);
    liste_ausgeben_rueckwaerts(anker);

    liste_loeschen(anker); // war: aus_liste_loeschen(anker, 99);
    liste_ausgeben(anker);
    liste_ausgeben_rueckwaerts(anker);

    for (int i = 0; i < laenge; i++) in_liste_einfuegen(anker, i * i, 9999);

    liste_ausgeben(anker);
    liste_ausgeben_rueckwaerts(anker);

    in_liste_einfuegen(anker, -1, 0);
    in_liste_einfuegen(anker, 24, 25);
    in_liste_einfuegen(anker, 80, 81);
    in_liste_einfuegen(anker, 99, 9999);

    liste_ausgeben(anker);
    liste_ausgeben_rueckwaerts(anker);

    aus_liste_loeschen(anker, 24);
    aus_liste_loeschen(anker, 80);

    liste_ausgeben(anker);
    liste_ausgeben_rueckwaerts(anker);
    liste_loeschen(anker);
    liste_ausgeben(anker);
    liste_ausgeben_rueckwaerts(anker);

//    system("PAUSE");
    return 0;
}