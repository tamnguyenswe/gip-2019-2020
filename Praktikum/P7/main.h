//
// Created by presariohg on 23/11/2019.
//

#ifndef P7_MAIN_H
#define P7_MAIN_H

#include <string>
#include <iostream>

using namespace std;

void parse_gesamtausdruck(string & input, int & pos);
void parse_ausdruck(string & input, int & pos);
void parse_term(string & input, int & pos);
void parse_operand(string & input, int & pos);
void parse_number(string & input, int & pos);

bool expect(char c, string & input, int pos);
void match(char c, string & input, int & pos);

#endif //P7_MAIN_H
