#include "main.h"

bool expect(char c, string & input, int pos) {
    cout << "Teste auf das Zeichen " << c << endl;
    if (unsigned(pos) >= input.length()) {
        cout << "Aber schon am Ende der Eingabe-Zeichenkette angelangt.\n";
        return false;
    }
    if (input.at(pos) == c) {
        cout << "Zeichen " << c << " gefunden." << endl;
        return true;
    } else {
        cout << "Test auf " << c << " nicht erfolgreich. Stattdessen " << input.at(pos) << " gesehen.\n";
        return false;
    }
}

void match(char c, string & input, int & pos) {
    cout << "Betrete match() fuer das Zeichen " << c << endl;
    if (unsigned(pos) >= input.length()) {
        cout << "Input-Zeichenkette zu kurz. Erwarte noch das Zeichen " << c << endl;
        cout << "Verlasse match(): " << endl;
        return;
    }
    if (input.at(pos) != c) {
        cout << "Fehler: an Position " << pos << " erwarte " << c << " und sehe " << input.at(pos) << endl;
        cout << "Verlasse match(): " << endl;
        return;
    }
    pos++;
    cout << "Zeichen " << c << " konsumiert.\n";
    cout << "Verlasse match() fuer das Zeichen " << c << endl;
}

void parse_gesamtausdruck(string & input, int & pos) {
    cout << "Betrete parse_gesamtausdruck()\n";
    parse_ausdruck(input, pos);

    // read the last '.' char
    match('.', input, pos);
    cout << "Verlasse parse_gesamtausdruck()\n";
}

void parse_number(string & input, int & pos) {
    cout << "Betrete parse_number()\n";

    // find if number from 0 to 9 matches or not
    for (int i = 0; i <= 9; i++) {
        if (expect('0' + i, input, pos)) {
            // found a number, move on
            match('0' + i, input, pos);
            break;
        }
    }

    cout << "Verlasse parse_number()\n";
}


void parse_ausdruck(string & input, int & pos) {
    cout << "Betrete parse_ausdruck()\n";
    parse_term(input, pos);

    while (expect('U', input, pos) || expect('O', input, pos)) {
        if (expect('U', input, pos)) {
            // found 'U', move to next char
            cout << "Betrete parse_ausdruck(): U Fall" << endl;
            match('U', input, pos);

            //find next term
            parse_term(input, pos);

            cout << "Verlasse parse_ausdruck(): U Fall" << endl;
        } else if (expect('O', input, pos)) {
            // found 'O', move to next char
            cout << "Betrete parse_ausdruck(): O Fall" << endl;
            match('O', input, pos);

            // find next term
            parse_term(input, pos);

            cout << "Verlasse parse_ausdruck(): O Fall" << endl;
        }
    }

    cout << "Verlasse parse_ausdruck()\n";
}


void parse_operand(string & input, int & pos) {
    cout << "Betrete parse_operand()\n";
    if (expect('(', input, pos)) {
        // found '(', move to next char
        cout << "Betrete parse_operand(): () Fall" << endl;
        match('(', input, pos);

        // find the ausdruck after the '(' char
        parse_ausdruck(input, pos);

        // after the ausdruck, read the ')' char, then move on
        match(')', input, pos);
        cout << "Verlasse parse_operand(): () Fall" << endl;
        return;
    } else {
        parse_number(input, pos);
    }


    cout << "Verlasse parse_operand()\n";
}


void parse_term(string & input, int & pos) {
    cout << "Betrete parse_term()\n";
    parse_operand(input, pos);

    while (expect('>', input, pos) || expect('<', input, pos)) {
        if (expect('>', input, pos)) {
            // found the '>', move to next char
            cout << "Betrete parse_term(): > Fall\n";
            match('>', input, pos);

            // found the other operand after the '<'
            parse_operand(input, pos);
            cout << "Verlasse parse_term(): > Fall\n";
        } else if (expect('<', input, pos)) {
            // found the '<', move to next char
            cout << "Betrete parse_term(): < Fall\n";
            match('<', input, pos);

            // found the other operand after the '<'
            parse_operand(input, pos);
            cout << "Verlasse parse_term(): < Fall\n";
        }
    }

    cout << "Verlasse parse_term()\n";
}


int main() {
    int pos = 0;
//    string input = "(4>3)U2<7.";
    string input = "";
    cout << "Bitte geben Sie die Zeichenkette ein: ";
    getline(cin, input);
    parse_gesamtausdruck(input, pos);

    if (pos != input.length()) cout << "Error! Noch Input-Zeichen uebrig." << endl;

    system("PAUSE");
    return 0;
}