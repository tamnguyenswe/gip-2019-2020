# cmake_minimum_required(VERSION <specify CMake version here>)
project(P7)

set(CMAKE_CXX_STANDARD 14)

add_executable(P7 main.cpp main.h)