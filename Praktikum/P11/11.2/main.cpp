#include <iostream>
#define CIMGGIP_MAIN
#include "CImgGIP05.h"
using namespace std;
using namespace cimg_library;

const int GRID_SIZE = 10; // Anzahl an Kaestchen in x- und y-Richtung
const int BOX_SIZE = 30;  // size der einzelnen Kaestchen (in Pixel)
const int BORDER = 20;    // Rand links und oben bis zu den ersten Kaestchen (in Pixel)

// Prototyp der Funktionen zum Vorbelegen des Grids ...
void grid_init(bool grid[][GRID_SIZE]);

int neighbors_count(int x, int y, bool grid[GRID_SIZE][GRID_SIZE]) {
    int neighbors = 0;

    const int LEFT_BORDER = 0;
    const int RIGHT_BORDER = GRID_SIZE - 1;
    const int UPPER_BORDER = 0;
    const int LOWER_BORDER = GRID_SIZE - 1;

    if ((x > LEFT_BORDER) && (y > UPPER_BORDER))
        if (grid[x - 1][y - 1])
            neighbors++;

    if (y > UPPER_BORDER)
        if (grid[x][y - 1])
            neighbors++;

    if ((x < RIGHT_BORDER) && (y > UPPER_BORDER))
        if (grid[x + 1][y - 1])
            neighbors++;

    if (x > LEFT_BORDER)
        if (grid[x - 1][y])
            neighbors++;

    if (x < RIGHT_BORDER)
        if (grid[x + 1][y])
            neighbors++;

    if ((x > LEFT_BORDER) && (y < LOWER_BORDER))
        if (grid[x - 1][y + 1])
            neighbors++;

    if (y < LOWER_BORDER)
        if (grid[x][y + 1])
            neighbors++;

    if ((x < RIGHT_BORDER) && (y < LOWER_BORDER)) 
        if (grid[x + 1][y + 1])
            neighbors++;

    return neighbors;
}

int main()
{
    bool grid[GRID_SIZE][GRID_SIZE] = { 0 };
    bool next_grid[GRID_SIZE][GRID_SIZE] = { 0 };

    // Erstes Grid vorbelegen ...
    grid_init(grid);

    while (gip_window_not_closed())
    {
        // Spielfeld anzeigen ...
		gip_stop_updates(); // ... schaltet das Neuzeichnen nach jeder Bildschirmänderung aus

        // Draw gray background
        short GRAY = 128;
        gip_background(GRAY);

        // Draw white playzone
        gip_draw_rectangle(BORDER, BORDER, 
                           GRID_SIZE * BOX_SIZE + BORDER, GRID_SIZE * BOX_SIZE + BORDER, white);

        // Draw cells
        for (int y = 0; y < GRID_SIZE; y++) 
            for (int x = 0; x < GRID_SIZE; x++) 
                if (grid[x][y]) {

                    int coord_x = x * BOX_SIZE + BORDER;
                    int coord_y = y * BOX_SIZE + BORDER;

                    gip_draw_rectangle(coord_x, coord_y,
                                       coord_x + BOX_SIZE, coord_y + BOX_SIZE, green);
                }
            
		gip_start_updates(); // ... alle Bildschirmänderungen (auch die nach dem gip_stop_updates() ) wieder anzeigen
        gip_sleep(1);

        // Berechne das naechste Spielfeld ...
        // Achtung; Für die Zelle (x,y) darf die Position (x,y) selbst *nicht*
        // mit in die Betrachtungen einbezogen werden.
        // Ausserdem darf bei zellen am rand nicht über den Rand hinausgegriffen
        // werden (diese Zellen haben entsprechend weniger Nachbarn) ...

        for (int y = 0; y <  GRID_SIZE; y ++)
            for (int x = 0; x < GRID_SIZE; x++) {
                int neighbors = neighbors_count(x, y, grid);

                // underpopulation
                if (neighbors < 2) {
                    next_grid[x][y] = false;
                    continue;
                }    

                // overpopulation
                if (neighbors > 3) {
                    next_grid[x][y] = false;
                    continue;
                }

                // reproduction
                if (neighbors == 3) {
                    next_grid[x][y] = true;
                    continue;
                }

                // else == 2, do nothing (dead continue being dead, alive continue being alive)
                next_grid[x][y] = grid[x][y];
            }
                
        // Kopiere das naechste Spielfeld in das aktuelle Spielfeld ...
        for (int y = 0; y <  GRID_SIZE; y ++)
            for (int x = 0; x < GRID_SIZE; x++) 
                grid[x][y] = next_grid[x][y];


    }
    return 0;
}

void grid_init(bool grid[][GRID_SIZE])
{
    int eingabe = -1;
    do {
        cout << "Bitte waehlen Sie die Vorbelegung des Grids aus:" << endl
            << "0 - Zufall" << endl
            << "1 - Statisch" << endl
            << "2 - Blinker" << endl
            << "3 - Oktagon" << endl
            << "4 - Gleiter" << endl
            << "5 - Segler 1 (Light-Weight Spaceship)" << endl
            << "6 - Segler 2 (Middle-Weight Spaceship)" << endl
            << "? ";
        cin >> eingabe;
        cin.clear();
        cin.ignore(1000, '\n');
    } while (eingabe < 0 || eingabe > 6);

    if (eingabe == 0)
    {
        // Erstes Grid vorbelegen (per Zufallszahlen) ...

        for (int y = 0; y < GRID_SIZE; y++) 
            for (int x = 0; x < GRID_SIZE; x++)
                grid[x][y] = gip_random(0, 1);

    }
    else if (eingabe == 1)
    {
        const int pattern_size = 3;
        char pattern[pattern_size][pattern_size] = 
        {
            { '.', '*', '.' },
            { '*', '.', '*' },
            { '.', '*', '.' },
        };
        for (int y = 0; y < pattern_size; y++) 
            for (int x = 0; x < pattern_size; x++) 
                if (pattern[y][x] == '*') 
                    grid[x][y+3] = true;
    }
    else if (eingabe == 2)
    {
        const int pattern_size = 3;
        char pattern[pattern_size][pattern_size] =
        {
            { '.', '*', '.' },
            { '.', '*', '.' },
            { '.', '*', '.' },
        };
        for (int y = 0; y < pattern_size; y++) 
            for (int x = 0; x < pattern_size; x++) 
                if (pattern[y][x] == '*') 
                    grid[x][y+3] = true;
    }
    else if (eingabe == 3)
    {
        const int pattern_size = 8;
        char pattern[pattern_size][pattern_size] =
        {
            { '.', '.', '.', '*', '*', '.', '.', '.' },
            { '.', '.', '*', '.', '.', '*', '.', '.' },
            { '.', '*', '.', '.', '.', '.', '*', '.' },
            { '*', '.', '.', '.', '.', '.', '.', '*' },
            { '*', '.', '.', '.', '.', '.', '.', '*' },
            { '.', '*', '.', '.', '.', '.', '*', '.' },
            { '.', '.', '*', '.', '.', '*', '.', '.' },
            { '.', '.', '.', '*', '*', '.', '.', '.' },
        };
        for (int y = 0; y < pattern_size; y++) 
            for (int x = 0; x < pattern_size; x++) 
                if (pattern[y][x] == '*') 
                    grid[x][y+1] = true;
    }
    else if (eingabe == 4)
    {
        const int pattern_size = 3;
        char pattern[pattern_size][pattern_size] =
        {
            { '.', '*', '.' },
            { '.', '.', '*' },
            { '*', '*', '*' },
        };
        for (int y = 0; y < pattern_size; y++) 
            for (int x = 0; x < pattern_size; x++) 
                if (pattern[y][x] == '*') 
                    grid[x][y+3] = true;
    }
    else if (eingabe == 5)
    {
        const int pattern_size = 5;
        char pattern[pattern_size][pattern_size] =
        {
            { '*', '.', '.', '*', '.' },
            { '.', '.', '.', '.', '*' },
            { '*', '.', '.', '.', '*' },
            { '.', '*', '*', '*', '*' },
            { '.', '.', '.', '.', '.' },
        };
        for (int y = 0; y < pattern_size; y++) 
            for (int x = 0; x < pattern_size; x++) 
                if (pattern[y][x] == '*') 
                    grid[x][y+3] = true;
    }
    else if (eingabe == 6)
    {
        const int pattern_size = 6;
        char pattern[pattern_size][pattern_size] =
        {
            { '.', '*', '*', '*', '*', '*' },
            { '*', '.', '.', '.', '.', '*' },
            { '.', '.', '.', '.', '.', '*' },
            { '*', '.', '.', '.', '*', '.' },
            { '.', '.', '*', '.', '.', '.' },
            { '.', '.', '.', '.', '.', '.' },
        };
        for (int y = 0; y < pattern_size; y++) 
            for (int x = 0; x < pattern_size; x++) 
                if (pattern[y][x] == '*') 
                    grid[x][y+3] = true;
    }
}
