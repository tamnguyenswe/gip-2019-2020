#include "sort.h"

void sortiere(int* a, int n) {
    for (int i = 1; i < n - 1; i++) {
        int einzusortieren = a[i];
        unsigned int j = i;

        while ((j > 0) &&
               (a[j - 1] > einzusortieren)) {
            a[j] = a[j - 1];
            j--;
        }

        a[j] = einzusortieren;
    }
}