#include <iostream>

void a3_1() {

    long date1, date2;

    int day, month, year;

    const int DAY_PER_MONTH = 30;
    const int DAY_PER_YEAR = 365;

    std::cout << "Bitte geben Sie den Tag des ersten Datums ein: ";
    std::cin >> day;
    date1 = day;

    std::cout << "Bitte geben Sie den Monat des ersten Datums ein: ";
    std::cin >> month;
    date1 += month * DAY_PER_MONTH;

    std::cout << "Bitte geben Sie das Jahr des ersten Datums ein: ";
    std::cin >> year;
    date1 += year * DAY_PER_YEAR;

    std::cout << "Bitte geben Sie den Tag des zweiten Datums ein: ";
    std::cin >> day;
    date2 = day;

    std::cout << "Bitte geben Sie den Monat des zweiten Datums ein: ";
    std::cin >> month;
    date2 += month * DAY_PER_MONTH;

    std::cout << "Bitte geben Sie das Jahr des zweiten Datums ein: ";
    std::cin >> year;
    date2 += year * DAY_PER_YEAR;

    if (date1 > date2) 
        std::cout << "Das zweite Datum liegt vor dem ersten Datum.\n";
    else if (date1 < date2)
        std::cout << "Das erste Datum liegt vor dem zweiten Datum.\n";
    else
        std::cout << "Beide Datumsangaben sind gleich.\n";
}

void a3_2() {
    int index_max, index_min, a[5];

    index_max = 0;
    index_min = 0;

    for (int i = 0; i < 5; i++) {
        std::cout << "Bitte geben Sie die " << i + 1 <<" Zahl ein: ? ";
        std::cin >> a[i];

        index_max = (a[index_max] < a[i]) ? index_max : i;
        index_min = (a[index_min] > a[i]) ? index_min : i;
    }

    std::cout << "Die " << index_min + 1 << ". Zahl war die kleinste der eingegebenen Zahlen und lautet: " << a[index_min] << "\n";
    std::cout << "Die " << index_max + 1 << ". Zahl war die groesste der eingegebenen Zahlen und lautet: " << a[index_max] << "\n";
}

int main() {

    char a, b;
    std::cout << "Bitte geben Sie die erste Ziffer ein: ? ";
    std::cin >> a;

    std::cout << "Bitte geben Sie die zweite Ziffer ein: ? ";
    std::cin >> b;

    if ((a == 'q') || (b == 'q')) {
        std::cout << "Das Programm wurde durch Eingabe von q beendet.\n";
        return 1;
    } else {
        std::cout << a << " + " << b << " = " << a - '0' + b - '0' << '\n';
    }
    return 0;
}