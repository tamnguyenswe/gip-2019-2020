#include <iostream>
#include <string>
#include <fstream>

struct Person {
    std::string name;
    std::string vorname;
    std::string birthday;
};

void parse_raw(std::string str, Person& person) {
    int index = 0;
    int temp = index;
    std::string name = "";
    std::string vorname = "";
    std::string birthday = "";

    //find name
    while (str[index++] != ',');
    for (int i = 0; i < index - 1; i++) name += str[i];
    index++; //skip the ' ' char after the ','

    //find vorname
    temp = index;
    while (str[index++] != ',');
    for (int i = temp; i < index - 1; i++) vorname += str[i];
    index++; //skip the ' ' char after the ','

    //find birthday
    for (int i = index; i < str.length(); i++) birthday += str[i];

    person.name = name;
    person.vorname = vorname;
    person.birthday = birthday;

}

int main() {
    Person people[5];

//    freopen("personendaten.txt", "r", stdin);
    std::ifstream people_ins("personendaten.txt");
    std::ifstream website_ins("webseitehtml.tmpl");
    std::ofstream website_out("webseite.html");

    for (int i = 0; people_ins; i++) {
        std::string str;
        std::getline(people_ins, str);
        if (str.length() <= 0) continue;
        parse_raw(str, people[i]);
    }

    do {
        std::string str;
        std::getline(website_ins, str);

        for (char c : str) {
            switch (c) {
                case '%':
                    for (Person person : people) {
                        website_out << "<b>" << person.name << "</b>, " << person.vorname << "<br/>\n";
                    }
                    break;
                case '$':
                    for (Person person : people) {
                        website_out << "<b>" << person.vorname << ' ' << person.name << "</b>, "
                                    << person.birthday << "<br/>\n";
                    }
                    break;
                default:
                    website_out << c;
            }
        }
        website_out << '\n';
    } while (website_ins);



    return 0;
}