#include <iostream>
#include <string>
using namespace std;

bool is_digit(char c) {
	return ((c >= '0') && (c <= '9'));
}

void print_3_chars_sudoku(int a[9][9], int row, int col1, int col2, int col3) {
	std::cout << ";" << a[row][col1] << ";" << a[row][col2] << ";" << a[row][col3];
}

void print_1_line_sudoku(int a[9][9], int row) {
	print_3_chars_sudoku(a, row, 0, 1, 2);
	std::cout << ";//";
	print_3_chars_sudoku(a, row, 3, 4, 5);
	std::cout << ";//";
	print_3_chars_sudoku(a, row, 6, 7, 8);
	std::cout << "\n";
}

void A5_1() {
	float input, sum_3_lasts, values_last[3], sum;

	sum_3_lasts = 0;
	sum = 0;
	values_last[0] = 0;
	values_last[1] = 0;
	values_last[2] = 0;

	do {
		do {
			std::cout << "Bitte geben Sie den neuen letzten Verbrauchswert ein: ? ";
			std::cin >> input;
		} while ((input < 0) || (input > 100));

		if (input == 0) break;

		values_last[0] = values_last[1];
		values_last[1] = values_last[2];
		values_last[2] = input;

		sum_3_lasts = values_last[0] + values_last[1] + values_last[2];
		sum += input;

		std::cout << "Der bisherige Gesamtverbrauch betraegt " << sum << "\n";
		std::cout << "Der Durchschnitt der letzten drei Verbrauchswerte betraegt " << sum_3_lasts / 3 << "\n";

	} while (input != 0);
}

void A5_2() {
	int a[6];
	for (int i = 1; i <= 5; i++) {
		do {
			std::cout << "Bitte geben Sie die " << i << ". Zahl ein: ? ";
			std::cin >> a[i];
		} while ((a[i] <= 0) || (a[i] > 9));
	}

	for (int i = 9; i > 0; i--) {
		std::cout << i;
		for (int j = 1; j <= 5; j++) {
			// a[j] > i, this line is below the *
			if (a[j] > i)
				std::cout << '+';
			// a[j] < i, this line is above the *
			else if (a[j] < i)
				std::cout << '.';
			// else this line is the *
			else
				std::cout << '*';
		}
		std::cout << '\n';
	}

	std::cout << " 12345\n";
}

void A5_3() {
	std::string eingabe[11];
	int sudoku[9][9], row, column;

	std::cout << "Bitte geben Sie das Sudoku ein: \n";
	row = -1;

	for (int i = 0; i < 11; i++) {
		std::cin >> eingabe[i];

		bool line_contains_digit = false;
		column = 0;

		for (int j = 0; j < eingabe[i].length(); j++) {
			char c = eingabe[i][j];

			if (is_digit(c)) {
				/* if this line contains digits and not marked yet, mark it
				   and start writing this line
				*/
				if (!line_contains_digit) {
					line_contains_digit = true;
					row++;
				}

				sudoku[row][column] = c - '0';

				column++;
			}
		}
	}

	std::cout << "\nDas Sudoku lautet: \n";

	// not using conditions on a 2-dimensional array, but shorter and more easy to read
	// print 3 first lines
	for (int i = 0; i < 3; i++)
		print_1_line_sudoku(sudoku, i);
	std::cout << "=======//=======//=======\n";

	// print 3 next lines
	for (int i = 3; i < 6; i++)
		print_1_line_sudoku(sudoku, i);
	std::cout << "=======//=======//=======\n";

	// print 3 last lines
	for (int i = 6; i < 9; i++)
		print_1_line_sudoku(sudoku, i);

}


int main() {

	A5_3();
	// system("PAUSE");
	return 0;
}
