#include "iostream"
#include "string.h"

const int ALPHABET_SIZE = 26;
const int HOURS_PER_DAY = 24;
const int MINUTES_PER_HOUR = 60;

void A4_1() {
    std::cout << "Merken Sie sich eine Zahl zwischen 1 (inklusive) und 100 (exklusive)\n\n";

    const int HIGH_END = 100;
    const int LOW_END = 1;

    bool is_found = false;

    int left = HIGH_END;
    int right = LOW_END;
    int pivot;

    char command;

    while (!is_found) {
        if (left == right) {
            std::cout << "You cheated!\n";
            break;
        }

        pivot = (left + right) / 2;
        std::cout << "Aktueller Ratebereich: " << right << " bis " << left << "\n";
        std::cout << "Ist " << pivot << " ihre Zahl?\n(j) ja: gleich,\n(k) nein: meine Zahl ist kleiner,\n(g) nein: meine Zahl ist groesser.\n ? ";
        std::cin >> command;

        switch (command) {
            case 'j':
                is_found = true;
                break;
            case 'k':
                // choose left side
                left = pivot;
                break;
            case 'g':
                // choose right side
                right = pivot;
                break;
        }
    }    
}

bool is_alphabet(char c) {
    return (((c - 'a' >= 0) && (c - 'z' <= 0)) || ((c - 'A' >= 0) && (c - 'Z' <= 0)));
}

char shift_char(char c, int key) {
    if (!is_alphabet(c + key)) {
        return c + key - ALPHABET_SIZE;
    }

    return c + key;
}

void A4_2() {
    std::string str;
    unsigned int key;

    std::cout << "Bitte geben Sie den zu verschluesselnden Text ein: ";
    std::getline(std::cin, str);

    std::cout << "Bitte geben Sie die Anzahl Verschiebepositionen ein (als positive ganze Zahl): ";
    std::cin >> key;

    // Trim down key
    key = key % ALPHABET_SIZE;

    for (int i = 0; i < str.length(); i++) {
        if (is_alphabet(str[i])) 
            str[i] = shift_char(str[i], key);
    }

    std::cout << str << "\n";
}

void A4_3() {

    int hour, minute, period;

    do {
        std::cout << "Bitte geben Sie die Stunden der Startuhrzeit ein: ";
        std::cin >> hour;
    } while ((hour < 0) || (hour > 23));

    do {
        std::cout << "Bitte geben Sie die Minuten der Startuhrzeit ein: ";
        std::cin >> minute;
    } while ((minute < 0) || (minute > 59));

    std::cout << "Der erste Bus faehrt also um " << hour << ":" << minute << " Uhr\n";

    do {
        std::cout << "Bitte geben Sie die Taktzeit in Minuten ein: ";
        std::cin >> period;
    } while ((period < 0) || (period > 180));

    while (hour < HOURS_PER_DAY) {

        // while not exceeds one hour, repeatly print time
        while (minute < MINUTES_PER_HOUR) {
            std::cout << hour << ":" << minute << " ";
            minute += period;
        }

        // one hour exceeded, increase hour & decrease minute count
        hour += minute / MINUTES_PER_HOUR;
        minute %= MINUTES_PER_HOUR;

        std::cout << "\n";
    }
}

int main() {

    A4_3();

    return 0;
}