#define CIMGGIP_MAIN
#include "CImgGIP05.h"

using namespace cimg_library;

const int LEFT_BORDER_X = 100;
const int LEFT_BORDER_Y = 100;

const int RIGHT_BORDER_X = 500;
const int RIGHT_BORDER_Y = 500;

const int RECTANGLE_SIZE = 50;
const int DELAY = 100;

bool is_valid_position_x(int x) {
    return  ((x >= LEFT_BORDER_X) && 
             (x <= RIGHT_BORDER_X - RECTANGLE_SIZE));
}

int main() {
    // Für das "blaue Spielfeld" ...
    // Für Ausdehnung und Position des weißen Quadrats ...
    int x = 200, y = 300;
    int delta = 10;
    gip_white_background();
    while (gip_window_not_closed()) {
        // Blaues "Spielfeld" neu zeichnen
        // (übermalt dann auch das letzte weiße Quadrat) ...
        gip_draw_rectangle(LEFT_BORDER_X, LEFT_BORDER_Y, // linke obere Ecke
                           RIGHT_BORDER_X, RIGHT_BORDER_Y, // rechte untere Ecke
                           blue);

        // Weißes Quadrat neu zeichnen an der Position x,y ...
        gip_draw_rectangle(x, y, // linke obere Ecke
                           x + RECTANGLE_SIZE, // rechte untere Ecke
                           y + RECTANGLE_SIZE, 
                           white); 

        // Nächste Position des weißen Quadrats berechnen.
        // Dabei berücksichtigen, dass das Quadrat von den Rändern des
        // blauen "Spielfelds" abprallen muss ...
        /* Ihr Code zur Berechnung der neuen Werte von x und y hier */

        // Check if next position is valid or not
        if (is_valid_position_x(x + delta)) {
            x+= delta;
        } else {
            delta = -delta;
            x+= delta;
        }

        gip_wait(DELAY);
    }
    return 0;
}