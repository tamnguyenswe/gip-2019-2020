#include "wortspiegel.h"

int main() {
    //This is a sentence to test with. This.is.another.sentence.
    std::string str;

    std::cout << "Bitte geben Sie den Text ein: ? ";
    std::getline(std::cin, str);
    
    char command;
    int position = 0;

    do {
        std::cout << str << '\n';
        print_position(position);

        std::cout << "Befehl (l: links, r: rechts, s: spiegeln, q: Ende) ?- ";
        std::cin >> command;

        switch (command) {
            // move left
            case 'l':
                if (position > 0) 
                    position--;
                break;
            // move right
            case 'r':
                if (position < str.length() - 1)
                    position++;
                break;
            // flip current word
            case 's':
                // skip if this char is not an alphabetical character
                if (is_alphabet(str[position])) {
                    int left = position;
                    int right = position;

                    // find current word's boundaries
                    while ((left > 0) && 
                           (is_alphabet(str[left - 1])))
                        left--;
                    
                    while ((right < str.length() - 2) &&
                           (is_alphabet(str[right + 1])))
                        right++;

                    std::string reversed_word = reverse(str, left, right);

                    // replace current word with the reversed one
                    int j = 0;
                    for (int i = left; i <= right; i++) {
                        str[i] = reversed_word[j];
                        j++;
                    }
                }
                break;
        }
    } while (command != 'q');

}