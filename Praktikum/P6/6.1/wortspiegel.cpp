#include <string>
#include <iostream>

void print_position(int position) {
    for (int i = 0; i < position; i++) {
        std::cout << ' ';
    }
    std::cout << "*\n";
}

std::string reverse(std::string str, int left, int right) {
    std::string result = "";
    for (int i = right; i >= left; i--) {
        result += str[i];
    }

    return result;
}

bool is_alphabet(char c) {
    return (((c >= 'a') && (c <= 'z')) ||
            ((c >= 'A') && (c <= 'Z')));
}

