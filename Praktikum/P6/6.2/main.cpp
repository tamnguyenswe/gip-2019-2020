#include <iostream>
#include <string>
using namespace std;

bool is_ok = true;

bool is_digit(char c) {
    return ((c >= '0') && (c <= '9'));
}

void check_horizontal_sudoku(int sudoku[9][9]) {
    // check lines, ugly algorithm with O(n^2) complexity, but still much better than O(n^3)
    for (int row = 0; row < 9; row++) {
        bool already_existing[10];
        bool is_valid = true;

        for (int i = 0; i <= 9; i++) already_existing[i] = false;

        for (int column = 0; column < 9; column++) {
            int this_number = sudoku[row][column];

            // if this number is already existing, report invalid
            if (already_existing[this_number]) {
                std::cout << "Zeile " << row << ": Zahl " << this_number << " kommt mehrfach vor.\n";
                is_valid = false;
            } else {
                already_existing[this_number] = true;
            }
        }

        if (!is_valid) {
            // check missing numbers
            is_ok = false;
            for (int number = 1; number <= 9; number++) {
                if (!already_existing[number])
                    std::cout << "Zeile " << row << ": Zahl " << number << " kommt nicht vor.\n";
            }
        }
    }
}

void check_vertical_sudoku(int sudoku[9][9]) {

    // check columns, ugly algorithm with O(n^2) complexity, but still much better than O(n^3)
    for (int column = 0; column < 9; column++) {
        bool already_existing[10];
        bool is_valid = true;

        for (int i = 0; i <= 9; i++) already_existing[i] = false;

        for (int row = 0; row < 9; row++) {
            int this_number = sudoku[row][column];

            // if this number is already existing, report invalid
            if (already_existing[this_number]) {
                std::cout << "Spalte " << column << ": Zahl " << this_number << " kommt mehrfach vor.\n";
                is_valid = false;
            } else {
                already_existing[this_number] = true;
            }
        }

        if (!is_valid) {
            // check missing numbers
            is_ok = false;
            for (int number = 1; number <= 9; number++) {
                if (!already_existing[number])
                    std::cout << "Spalte " << column << ": Zahl " << number << " kommt nicht vor.\n";
            }
        }
    }
}

void check_block_sudoku(int sudoku[9][9]) {
    int block_index = 0;
    for (int row = 0; row < 9; row += 3) {
        for (int column = 0; column < 9; column += 3) {
            int block[9];
            int index = 0;
            bool already_existing[10];

            for (int i = 0; i <= 9; i++) already_existing[i] = false;


            // extract this block from the big sudoku
            for (int i = row; i < row + 3; i++)
                for (int j = column; j < column + 3; j++) {
                    block[index] = sudoku[i][j];
                    index++;
                }

            for (index = 0; index < 9; index++) {
                int this_number = block[index];

                // if this number is already existing, report invalid
                if (already_existing[this_number]) {
                    is_ok = false;
                    std::cout << "Block " << block_index << ": Zahl " << this_number << " kommt mehrfach vor.\n";
                } else {
                    already_existing[this_number] = true;
                }
            }

            // check missing numbers
            for (int number = 1; number <= 9; number++) {
                if (!already_existing[number]) {
                    is_ok = false;
                    std::cout << "Block " << block_index << ": Zahl " << number << " kommt nicht vor.\n";
                }
            }
            block_index++;
        }
    }
}

int main() {
    std::string eingabe[11];
    int sudoku[9][9], row, column;

    std::cout << "Bitte geben Sie das Sudoku ein: \n";
    row = -1;

    for (int i = 0; i < 11; i++) {
        std::cin >> eingabe[i];

        bool line_contains_digit = false;
        column = 0;

        for (int j = 0; j < eingabe[i].length(); j++) {
            char c = eingabe[i][j];

            if (is_digit(c)) {
                /* if this line contains digits and not marked yet, mark it
                   and start writing this line
                */
                if (!line_contains_digit) {
                    line_contains_digit = true;
                    row++;
                }

                sudoku[row][column] = c - '0';

                column++;
            }
        }
    }

    check_vertical_sudoku(sudoku);
    check_horizontal_sudoku(sudoku);
    check_block_sudoku(sudoku);

    if (is_ok) {
        std::cout << "Das Sudoku ist gueltig.\n";
    }

    return 0;

}

/*
.5.1.4.|.1.6.9.|.7.2.3
.8.7.2.|.3.4.5.|.6.1.9
.9.6.3.|.2.1.7.|.5.4.8
-------|-------|-------
.4.2.8.|.1.3.4.|.9.5.7
.1.9.7.|.6.5.2.|.8.3.4
.4.3.5.|.7.9.8.|.1.6.2
-------|-------|-------
.2.4.6.|.9.7.1.|.3.8.5
.7.5.1.|.4.8.3.|.2.9.6
.3.8.9.|.5.2.6.|.4.7.1

.5.1.4.|.8.6.9.|.7.2.3
.8.7.1.|.3.4.2.|.6.1.9 
.9.6.3.|.2.1.7.|.5.3.8 
-------|-------|------- 
.4.2.8.|.5.3.4.|.6.5.7 
.1.9.7.|.6.5.2.|.8.3.4 
.4.3.5.|.7.9.8.|.1.6.2 
-------|-------|------- 
.2.4.6.|.9.7.1.|.3.8.5 
.7.5.1.|.4.8.3.|.2.9.6 
.3.8.7.|.5.2.8.|.4.7.9
*/