#include "MyString2.h"
#include <iostream>

int main() {
    MyString2 s;
    MyString2 s2 = std::string{"def"};

    // s2 = s;
    // std::cout << (s2.get_anker() != s.get_anker()) << "\n";
    // std::cout << s2.to_string() << "\n";
    std::cout << s2 << "\n";

    return 0;
}