#include "MyString2.h"
#include <iostream>


CharListenKnoten * MyString2::get_anker() const {
    return this->anker;
}


void MyString2::set_anker(CharListenKnoten * anker) {
    this->anker = anker;
    //maybe should change length?
}


void MyString2::append(char p_data) {
    CharListenKnoten * new_node = new CharListenKnoten();

    new_node->set_data(p_data);

    // find last node

    if (anker == nullptr) //empty string
        anker = new_node;
    else { //not empty string
        CharListenKnoten * last = this->anker;
        for (; last->get_next() != nullptr; last = last->get_next());

        last->set_next(new_node);
    }
    this->str_length++;
}


void MyString2::delete_all() {
    if (this->anker != nullptr)
        this->delete_recursive(this->anker);

    this->set_anker(nullptr);
    this->str_length = 0;
}


unsigned int MyString2::length() const {
    return this->str_length;
}


void MyString2::delete_recursive(CharListenKnoten * current_node) {
    if (current_node->get_next() == nullptr) {
        delete current_node;
        return;
    } else {
        delete_recursive(current_node->get_next());
    }
}


char MyString2::at(unsigned int position) {
    CharListenKnoten * node = this->anker;

    if (this->anker == nullptr) 
        return '\0';

    while (position > 0) {
        position--;

        if (node->get_next() == nullptr) {
            return '\0';
        } else {
            node = node->get_next();
        }
    }

    return node->get_data();
}


std::string MyString2::to_string() const {
    std::string string = "";

    CharListenKnoten * node = this->anker;
    if (node == nullptr) {
        // do nothing
    } else {
        while(node != nullptr) {
            string += node->get_data();
            node = node->get_next();
        }
    }

    return string;
}

// void MyString2::print() {


MyString2 MyString2::operator+(char c) const {
    MyString2 new_string = *this;
    new_string.append(c);
    return new_string;
}


MyString2& MyString2::operator=(const MyString2 & other) {
    this->delete_all();
    this->anker = other.deep_copy();
    this->str_length = other.length();

    return *this;
}


std::ostream& operator<<(std::ostream & out, const MyString2 & string) {
    for (CharListenKnoten * chr = string.get_anker(); chr != nullptr; chr = chr->get_next()) {
        out << chr->get_data();
    }

    return out;
}


MyString2::MyString2() {
    this->anker = nullptr;
    this->str_length = 0;
}


MyString2::MyString2(const std::string string) {
    this->anker = nullptr;
    this->str_length = 0;
    for (char c : string) {
        this->append(c);
    }
}


MyString2::MyString2(const MyString2 & other) {
    this->anker = other.deep_copy();
    this->str_length = other.length();
}


CharListenKnoten * MyString2::deep_copy() const{
    if (this->anker == nullptr) {
        return nullptr;
    } else {
        // make a new anker
        CharListenKnoten * other_anker = new CharListenKnoten();
        other_anker->set_data(this->anker->get_data());

        // copy all other nodes
        CharListenKnoten * this_node = this->anker;
        CharListenKnoten * other_node = other_anker;

        while (this_node->get_next() != nullptr) {
            CharListenKnoten * next_node = new CharListenKnoten();
            next_node->set_data(this_node->get_next()->get_data());

            other_node->set_next(next_node);
            other_node = next_node;
            this_node = this_node->get_next();
        }

        return other_anker;
    }
}


MyString2::~MyString2() {
    this->delete_all();
}