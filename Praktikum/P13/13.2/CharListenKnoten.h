#ifndef LINKED_LIST
#define LINKED_LIST
#include <string>

class CharListenKnoten {
private:
    char data;
    CharListenKnoten* next;
public:
    char get_data() {
        return data;
    }  

    void set_data(char data) {
        this->data = data;
    }

    CharListenKnoten * get_next() {
        return this->next;
    }

    void set_next(CharListenKnoten * next) {
        this->next = next;
    }

    CharListenKnoten() {
        this->next = nullptr;
    }
};

#endif