#include "CharListenKnoten.h"

class MyString2 {
private:
    CharListenKnoten * anker;

    void delete_recursive(CharListenKnoten * current_node);
    unsigned int str_length;

public:
    CharListenKnoten * get_anker() const;
    CharListenKnoten * deep_copy() const;

    void set_anker(CharListenKnoten * anker);

    void append(char p_data);

    void delete_all();

    unsigned int length() const;

    char at(unsigned int position);

    std::string to_string() const;

    // void print();

    MyString2();
    MyString2(const std::string s);
    MyString2(const MyString2 & other);

    MyString2 operator+(char c) const;
    MyString2& operator=(const MyString2 & other);
    friend std::ostream& operator<<(std::ostream &out, const MyString2 &string);

    ~MyString2();
};