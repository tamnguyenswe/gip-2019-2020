#include "MyFilledRectangle.h"

void MyFilledRectangle::draw() {
    MyRectangle::draw();

    if (y2 - y1 <= 4) return;

    for (int y = y1 + 2; y < y2 - 2; y++) {
        gip_draw_line(x1 + 2, y, x2 - 2, y, red);
    }

}
