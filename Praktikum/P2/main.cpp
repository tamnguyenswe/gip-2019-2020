#include "iostream"

// convert F degree to C degree
double f_to_c(double temperature_c) {
    return temperature_c * 1.8 + 32;
}

double m_to_ft(double meter) {
    const double METER_TO_FEET = 3.2808;

    return meter * METER_TO_FEET;
}

double eur_to_usd(double eur) {
    const double EUR_TO_USD = 1.2957;
    return eur * EUR_TO_USD;
}


void A2_1() {
    char c;

    std::cout << "Bitte geben Sie den Buchstaben ein: ? ";
    std::cin >> c;

    const int FIRST_POSITION = 96;

    std::cout << "Der Buchstabe d hat die Position " << int(c) - FIRST_POSITION << " im Alphabet.\n";
}

void A2_2() {
    double temp;

    std::cout << "Bitte geben Sie die Temperatur in Grad Celsius ein: ? ";
    std::cin >> temp;

    std::cout << "Die Temperatur in Fahrenheit lautet: " << f_to_c(temp) << "\n";
}

void A2_3() {
    double meter;
    std::cout << "Bitte geben Sie die Laenge in Metern ein: ? ";
    std::cin >> meter;

    std::cout << "Die Laenge in Fuss lautet: " << m_to_ft(meter) << "\n";
}

void A2_4() {
    double euro;
    std::cout << "Bitte geben Sie die Geldmenge in Euro ein: ? ";
    std::cin >> euro;
    std::cout << "Die Geldmenge in US Dollar lautet: " << eur_to_usd(euro) << "\n";
}

void A2_5() {
    double input, result;
    int command;
    // int command = 3;
    // input = 34.5;

    std::cout << "Ihre Eingabe: ? ";
    std::cin >> input;

    std::cout << "\nIhre Auswahl der Umwandlung: \n";
    std::cout << " 1 - Celsius in Fahrenheit \n";
    std::cout << " 2 - Meter in Fuss \n";
    std::cout << " 3 - Euro in US Dollar \n";


    std::cin >> command;

    result = (command == 1) * f_to_c(input) +
             (command == 2) * m_to_ft(input) +
             (command == 3) * eur_to_usd(input);

    std::cout << "Das Ergebnis lautet: " << result << "\n";
}

int main() {
    A2_5();

    return 0;
}