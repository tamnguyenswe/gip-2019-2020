#include "suchen.h"


/**
 * search for a substring in a string
 * @param text the string to search
 * @param zkette the substring to search with
 * @return the first position of the substring, -1 if not found
 */
int zeichenkette_suchen(const char * text, const char * zkette) {
    const char * sub_string = zkette;
    int zkette_begin = 0;

    while (*text != '\0') {

        if (*text == *sub_string) {
            // use a temporary pointer in order to not change the main text pointer
            const char * temp_text = text;
            while (true) {
                temp_text++;
                sub_string++;

                // reached the end of the substring, which means this substring was found. return its first index
                if (*sub_string == '\0') {
                    return zkette_begin;
                }

                // if current characters are different, reset the substring pointer, then break the loop
                if (*temp_text != *sub_string) {
                    sub_string = zkette;
                    break;
                }
            }
        }

        zkette_begin++;
        text++;
    }

    return -1;
}

