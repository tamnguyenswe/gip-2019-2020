#include <iostream>

/**
 * search for a substring in a string
 * @param text the string to search
 * @param zkette the substring to search with
 * @return the first position of the substring, -1 if not found
 */
int zeichenkette_suchen(const char * text, const char * zkette);