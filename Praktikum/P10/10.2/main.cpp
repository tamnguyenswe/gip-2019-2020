#define CATCH_CONFIG_RUNNER

#include "catch.h"
#include "suchen.h"
#include <iostream>

using namespace std;

int main() {
    if (Catch::Session().run()) {
//        system("PAUSE");
        return 1;
    }

    char text[20];
    char sub_string[20];

    std::cout << "Bitte geben Sie den Text ein: ";
    std::cin.getline(text, 20);

    std::cout << "Bitte geben Sie die zu suchende Zeichenkette ein: ";
    std::cin.getline(sub_string, 20);

    int position = zeichenkette_suchen(text, sub_string);
    if (position == -1)
        std::cout << "Die Zeichenkette '" << sub_string << "' ist NICHT in dem Text '" << text << "' enthalten.\n";
    else {
        std::cout << "Die Zeichenkette '" << sub_string << "' ist in dem Text '" << text << "' enthalten.\n";
        std::cout << "Sie startet ab Zeichen " << position << " (bei Zaehlung ab 0).\n";
    }
//    system("PAUSE");
    return 0;
}