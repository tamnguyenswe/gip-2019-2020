#include <iostream>
#include "binaerer_suchbaum.h"


/**
 * check if this node is full (has 2 children)
 * @param node the node to check
 * @return true if the node has 2 children, false otherwise
 */
bool suchbaum::is_full_node(BaumKnoten * node) {
    return ((node->left != nullptr) &&
            (node->right) != nullptr);
}


/**
 * check if this node is available for a new value
 * @param node the node to check
 * @return true if this node is empty, false otherwise
 */
bool suchbaum::is_empty(BaumKnoten * node) {
    return (node == nullptr);
}


/**
 * add a node to the tree
 * @param data the new node's data
 * @param root the tree's root
 */
void suchbaum::einfuegen(int data, BaumKnoten * root) {
    auto * current_node = root;

    while (true) {
        // not adding duplicated values
        if (data == current_node->data) return;

        bool is_go_right = (data > current_node->data);

        // if this node is full, move to its child node
        if (is_full_node(current_node)) {
            current_node = (is_go_right) ? current_node->right : current_node->left;
            continue;
        }

        // else this node still has free places

        if (is_go_right)
            if (is_empty(current_node->right)) {
                auto * new_node = new BaumKnoten;
                new_node->data = data;
                new_node->parent = current_node;
                new_node->left = nullptr;
                new_node->right = nullptr;

                current_node->right = new_node;
            } else {
                current_node = current_node->right;
                continue;
            }
        else
            if (is_empty(current_node->left)) {
                auto * new_node = new BaumKnoten;

                new_node->data = data;
                new_node->parent = current_node;
                new_node->left = nullptr;
                new_node->right = nullptr;

                current_node->left = new_node;
            } else {
                current_node = current_node->left;
                continue;
            }

        return;
    }
}


/**
 * print out a tree, right to left
 * @param root: the root of the tree
 * @param tiefe: the depth of current root
 */
void suchbaum::knoten_ausgeben(BaumKnoten * node, int tiefe) {

    // print out this node's right child and its children
    if (!is_empty(node->right))
        knoten_ausgeben(node->right, tiefe + 1);

    // print out the '+'s and this node's data
    for (int i = 0; i < tiefe; i++) std::cout << "++";
    std::cout << node->data << "\n";

    if (!is_empty(node->left))
        knoten_ausgeben(node->left, tiefe + 1);
}


int main() {
    int data;
    auto * root = new suchbaum::BaumKnoten;

    std::cout << "Leerer Baum." << std::endl;

    root->parent = nullptr;
    root->left = nullptr;
    root->right = nullptr;

    std::cout << "Naechster Wert (99 beendet): ? ";
    std::cin >> data;

    // exit if read 99
    if (data == 99) {
        std::cout << "Leerer Baum.\n";
        return 0;
    }

    root->data = data;

    while (true) {
        std::cout << "Naechster Wert (99 beendet): ? ";
        std::cin >> data;

        if (data == 99) break;

        suchbaum::einfuegen(data, root);
    }

    suchbaum::knoten_ausgeben(root, 0);

    return 0;
}
