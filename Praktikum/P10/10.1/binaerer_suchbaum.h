namespace suchbaum {
    struct BaumKnoten {
        int data;

        BaumKnoten * parent;
        BaumKnoten * left;
        BaumKnoten * right;
    };


    /**
     * add a node to the tree
     * @param data the new node's data
     * @param root the tree's root
     */
    void einfuegen(int data, BaumKnoten * root);


    /**
     * print out a tree, right to left
     * @param root: the root of the tree
     * @param tiefe: the depth of current root
     */
    void knoten_ausgeben(BaumKnoten * root, int tiefe);


    /**
     * check if this node is full (has 2 children)
     * @param node the node to check
     * @return true if the node has 2 children, false otherwise
     */
    bool is_full_node(BaumKnoten * node);


    /**
     * check if this node is available for a new value
     * @param node the node to check
     * @return true if this node is empty, false otherwise
     */
    bool is_empty(BaumKnoten * node);

}