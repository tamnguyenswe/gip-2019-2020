#include "iostream"

int main() {
    int a[9], counter[7];

    for (int i = 0; i < 9; i++) {
        do {
            std::cout << "Bitte geben Sie die " << i + 1 << ". Zahl ein: ? ";
            std::cin >> a[i];
        } while ((a[i] < 1) || (a[i] > 6));

        if (i < 7) counter[i] = 0;
    }

    for (int i = 0; i < 9; i++) {
        std::cout << "Die " << i + 1 << ". eingegebene Zahl lautete: " << a[i] << "\n";
        if (a[i] < 7) counter[a[i]]++;
    }

    for (int i = 1; i <= 6; i++) {
        std::cout << "Die Zahl " << i << " wurde " << counter[i] << " mal eingegeben.\n";
    }

    return 0;
}