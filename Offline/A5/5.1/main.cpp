#include "iostream"

int main() {
    int a[9];

    for (int i = 0; i < 9; i++) {
        do {
            std::cout << "Bitte geben Sie die " << i + 1 << ". Zahl ein: ? ";
            std::cin >> a[i];
        } while ((a[i] < 1) || (a[i] > 6));
    }

    for (int i = 0; i < 9; i++) {
        std::cout << "Die " << i + 1 << ". eingegebene Zahl lautete: " << a[i] << "\n";
    }

    return 0;
}