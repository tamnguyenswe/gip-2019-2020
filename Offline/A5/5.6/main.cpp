#include "iostream"

int main() {
    int a[6], max, min;

    for (int i = 1; i < 6; i++) {
        std::cout << "Bitte geben Sie die " << i << ". Zahl ein: ? ";
        std::cin >> a[i];
    }

    max = a[1];
    min = a[1];

    for (int i = 1; i < 6; i++) {
        std::cout << "Die " << i << ". eingegebene Zahl lautete: " << a[i] << "\n";
        max = (max > a[i])? max : a[i];
        min = (min < a[i])? min : a[i];
    }

    std::cout << "Die kleinste eingegebene Zahl lautete: " << min << "\n";
    std::cout << "Die groesste eingegebene Zahl lautete: " << max << "\n";

    return 0;
}