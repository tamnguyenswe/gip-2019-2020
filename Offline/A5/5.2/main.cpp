#include "iostream"

int main() {
    int a[6], target;

    for (int i = 0; i < 6; i++) {
        do {
            std::cout << "Bitte geben Sie die " << i + 1 << ". Zahl ein: ? ";
            std::cin >> a[i];
        } while ((a[i] < 1) || (a[i] > 6));
    }

    std::cout << "Bitte geben Sie die Suchzahl ein: ? ";
    std::cin >> target;

    for (int i = 0; i < 9; i++) {
        if (a[i] == target) {
            std::cout << "Die Suchzahl kam unter den Eingaben vor.\n";
            return 0;
        }
    }

    std::cout << "Die Suchzahl kam NICHT unter den Eingaben vor.\n";

    return 0;
}