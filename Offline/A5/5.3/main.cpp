#include "iostream"

int main() {
    int a[4], target, counter;

    counter = 0;

    for (int i = 0; i < 4; i++) {
        std::cout << "Bitte geben Sie die " << i + 1 << ". Zahl ein: ? ";
        std::cin >> a[i];
    }

    std::cout << "Bitte geben Sie die Vergleichszahl ein: ? ";
    std::cin >> target;

    for (int i = 0; i < 4; i++) {
        if (a[i] == target) {
            counter++;
        }
    }

    std::cout << counter << " Eingabezahlen waren gleich der Vergleichszahl.\n";
    std::cout << 4 - counter << " Eingabezahlen waren ungleich der Vergleichszahl.\n";

    return 0;
}