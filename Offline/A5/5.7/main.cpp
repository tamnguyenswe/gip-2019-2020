#include "iostream"
#include "string.h"

int char_to_int(char c) {
    return c - '0';
}

// return x^n;
long power(int x, int n) {
    long result = 1;

    for (int i = 0; i < n; i++)
        result *= x;

    return result;
}

long str_to_long(std::string str) {
    int exponent = 0;
    long result = 0;

    int index = str.length() - 1;
    while (index >= 0) {
        int this_digit = char_to_int(str[index]);

        result += this_digit * power(10, exponent);

        index--;
        exponent++;
    }

    return result;
}

int main() {
    std::string str;

    std::cout << "Bitte die Zahl oder das Wort 'ende' eingeben: ? ";
    std::cin >> str;

    if (str.compare("ende") == 0) {
        std::cout << "Das Programm beendet sich jetzt.\n";
        return 0;
    }

    long number = str_to_long(str);
    std::cout << "Der doppelte Wert betraegt: " << number * 2 << '\n'; 


    return 0;
}