#include "iostream"

int main() {
    int a[9], counter;

    counter = 0;

    for (int i = 0; i < 9; i++) {
        do {
            std::cout << "Bitte geben Sie die " << i + 1 << ". Zahl ein: ? ";
            std::cin >> a[i];
        } while ((a[i] < 1) || (a[i] > 6));

        bool duplicate_found = false;

        for (int j = 0; j < i; j ++) 
            if (a[j] == a[i]) 
                duplicate_found = true;

        if (duplicate_found) continue;

        counter++;
    }

    std::cout << "In der Eingabe kamen " << counter << " unterschiedliche Zahlen vor.\n";

    return 0;
}