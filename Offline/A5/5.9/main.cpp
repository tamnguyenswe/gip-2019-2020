#include <string>
#include <iostream>
using namespace std;

string trimme(string source) {
    string result = "";

    int left, right;
    left = 0;
    right = source.length() - 1;

    while (source[left] == '+') 
        left++;

    while (source[right] == '+')
        right--;

    for (int i = left; i <= right; i++) {
        result += source[i];
    }

    return result;
}

int main() {     
    string s = "";
    cout << "Bitte geben Sie die Textzeile ein: ? ";     
    getline(cin, s);     
    cout << "Vorher: " << s << endl;     
    cout << "Nachher: " << trimme(s) << endl;     
    // system("PAUSE");     
    return 0;   
}