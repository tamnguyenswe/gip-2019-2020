#include<iostream>
#include<string>
using namespace std; 

string validate(char c, char target, string replacement) {
    if (c == target) {
        return replacement;
    } else {
        string result = "";
        result += c;
        return result;
    }
}

string ersetze(string source, char target, string replacement) {
    string result = "";
    for (int i = 0; i < source.length(); i++) {
        result += validate(source[i], target, replacement);
    }

    return result;
}

int main() { 
    string s1 = "", s2 = "";  
    char c; 

    cout << "Bitte geben Sie die Textzeile ein: ? "; 
    getline(cin, s1); 

    cout << "Bitte geben Sie das zu ersetzende Zeichen ein: ? "; 
    cin >> c; 
    cin.ignore(); 

    cout << "Bitte geben Sie den einzusetzenden Text ein: ? "; 
    getline(cin, s2); 

    cout << "Ergebnis: " << ersetze(s1, c, s2) << endl; 
    // system("PAUSE"); 
    return 0;
}