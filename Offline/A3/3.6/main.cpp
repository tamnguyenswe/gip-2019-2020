#include "iostream"

int main() {
    int max, min, temp;

    std::cout << "Bitte geben Sie die 1. Zahl ein: ? ";
    std::cin >> max;
    min = max;

    std::cout << "Bitte geben Sie die 2. Zahl ein: ? ";
    std::cin >> temp;
    max = (max > temp) ? max : temp;
    min = (min < temp) ? min : temp;

    std::cout << "Bitte geben Sie die 3. Zahl ein: ? ";
    std::cin >> temp;
    max = (max > temp) ? max : temp;
    min = (min < temp) ? min : temp;

    std::cout << "Die kleinste eingegebene Zahl lautet: " << min << "\n";
    std::cout << "Die groesste eingegebene Zahl lautet: " << max << "\n";

    return 0;
}