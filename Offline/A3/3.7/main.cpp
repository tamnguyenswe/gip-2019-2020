#include "iostream"

int main() {
    int n, counter;

    counter = 0;

    std::cout << "Bitte geben Sie die 1. Zahl ein: ? ";
    std::cin >> n;
    if (n == 99) counter++;

    std::cout << "Bitte geben Sie die 2. Zahl ein: ? ";
    std::cin >> n;
    if (n == 99) counter++;

    std::cout << "Bitte geben Sie die 3. Zahl ein: ? ";
    std::cin >> n;
    if (n == 99) counter++;

    std::cout << "Bitte geben Sie die 4. Zahl ein: ? ";
    std::cin >> n;
    if (n == 99) counter++;

    std::cout << counter << " Eingabezahlen waren gleich der Vergleichszahl 99.\n";
    std::cout << 4 - counter << " Eingabezahlen waren ungleich der Vergleichszahl 99.\n";

    return 0;
}