#include "iostream"
#include "string.h"

int main() {
    std::string str;

    std::cout << "Bitte geben Sie die Zeichenkette (6 Zeichen) ein: ? ";
    std::cin >> str;

    bool is_palindrom = true;
    int end = str.length();
    for (int i = 0; i < end/2; i++ ){
        if (str[i] != str[end - i - 1]) {
            is_palindrom = false;
            break;
        }
    }

    if (is_palindrom) 
        std::cout << "Das eingegebene Wort ist ein Palindrom.\n";
    else 
        std::cout << "Das eingegebene Wort ist KEIN Palindrom.\n";

    return 0;
}