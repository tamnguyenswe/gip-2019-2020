#include "iostream"
#include "locale"

int main() {
    char c;

    std::cout << "Bitte geben Sie das Zeichen ein: ? ";
    std::cin >> c;
    
    if (isdigit(c)) {
        std::cout << c << " + 5 = " << c - '0' + 5 << "\n";
    } else if (c == 'e') {
        std::cout << "Das Programm beendet sich jetzt.\n";
    } else {
        std::cout << "Weder 'e' noch Ziffer\n";
    }

    return 0;
}