#include "iostream"

int main() {
    int hour;

    std::cout << "Bitte geben Sie die Stunde der aktuellen Uhrzeit ein: ? ";
    std::cin >> hour;

    switch (hour) {   
        case 23: case 0: case 1: case 2: case 3: case 4: case 5:
            std::cout << "Gute Nacht.\n";
            break;
        
        case 6: case 7: case 8: case 9: case 10:
            std::cout << "Guten Morgen.\n";
            break;

        case 11: case 12: case 13:
            std::cout << "Mahlzeit.\n";
            break;

        case 14: case 15: case 16: case 17: 
            std::cout << "Guten Tag.\n";
            break;

        case 18: case 19: case 20: case 21: case 22: 
            std::cout << "Guten Abend.\n";
            break;

        default:
            std::cout << "Keine erlaubte Stundenangabe.\n";
            
    }

    return 0;
}