#include "iostream"

int main() {
    char c;

    std::cout << "Bitte geben Sie das Zeichen ein: ? ";
    std::cin >> c;

    if ((int(c) - 'a' < 0) || (int(c) - 'a' > 25)) {
        std::cout << "KEIN Kleinbuchstabe (a-z).\n";
    } else {
        std::cout << "Es wurde ein Kleinbuchstabe (a-z) eingegeben.\n";
    }

    return 0;
}