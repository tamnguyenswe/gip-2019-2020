#include "iostream"
#include "locale"

int main() {
    char c;
    std::cout << "Bitte geben Sie das Zeichen ein: ? ";
    std::cin >> c;

    c = tolower(c);

    if (c == 'j') {
        std::cout << "Es handelt sich um eine Ja Eingabe.\n";
    } else if (c == 'n') {
        std::cout << "Es handelt sich um eine Nein Eingabe.\n";
    } else {
        std::cout << "Es handelt sich um eine sonstige Eingabe.\n";
    }

    return 0;
}