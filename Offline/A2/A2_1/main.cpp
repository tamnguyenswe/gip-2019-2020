#include "iostream"

int main() {
    double n;

    std::cout << "Bitte geben Sie die Seitenlaenge ein (in cm): ? ";
    std::cin >> n;

    double circumference = n*4;
    double area = n*n;

    std::cout << "Der Umfang des Quadrats betraegt (in cm): " << circumference << std::endl;    
    std::cout << "Die Flaeche des Quadrats betraegt (in cm*cm): " << area << std::endl;

    return 0;
}