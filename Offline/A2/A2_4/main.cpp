#include "iostream"

int main() {
    int hour, minute, second;
    long timeStamp1, timeStamp2;
    const int SECOND_PER_MINUTE = 60;
    const int SECOND_PER_HOUR = 3600;

    std::cout << "Bitte geben Sie die Stundenzahl der ersten Uhrzeit ein: ? ";
    std::cin >> hour;
    std::cout << "Bitte geben Sie die Minutenzahl der ersten Uhrzeit ein: ? ";
    std::cin >> minute;
    std::cout << "Bitte geben Sie die Sekundenzahl der ersten Uhrzeit ein: ? ";
    std::cin >> second;

    timeStamp1 = hour * SECOND_PER_HOUR + minute * SECOND_PER_MINUTE + second;
    std::cout << hour << ":" << minute << " Uhr und " << second << " Sekunden sind in Sekunden seit Mitternacht: " << timeStamp1 << "\n\n";

    std::cout << "Bitte geben Sie die Stundenzahl der zweiten Uhrzeit ein: ? ";
    std::cin >> hour;
    std::cout << "Bitte geben Sie die Minutenzahl der zweiten Uhrzeit ein: ? ";
    std::cin >> minute;
    std::cout << "Bitte geben Sie die Sekundenzahl der zweiten Uhrzeit ein: ? ";
    std::cin >> second;

    timeStamp2 = hour * SECOND_PER_HOUR + minute * SECOND_PER_MINUTE + second;
    std::cout << hour << ":" << minute << " Uhr und " << second << " Sekunden sind in Sekunden seit Mitternacht: " << timeStamp2 << "\n\n";

    second = timeStamp2 - timeStamp1;

    hour = second / SECOND_PER_HOUR;
    second -= hour * SECOND_PER_HOUR;
    minute = second / SECOND_PER_MINUTE;
    second -= minute * SECOND_PER_MINUTE;

    std::cout << "Die Differenz zwischen den beiden Uhrzeiten betraegt:\n";
    std::cout << hour <<" Stunden, " << minute << " Minuten und " << second <<" Sekunden.\n";

    return 0;
}