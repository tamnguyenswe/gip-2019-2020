#include "iostream"

int main() {
    double sum, first, second, third;

    std::cout << "Bitte geben Sie die Gesamtzahl der abgegebenen gueltigen Stimmen ein: ? ";
    std::cin >> sum;
    std::cout << "Bitte geben Sie die Anzahl Stimmen des ersten Kandidaten ein: ? ";
    std::cin >> first;
    std::cout << "Bitte geben Sie die Anzahl Stimmen des zweiten Kandidaten ein: ? ";
    std::cin >> second;

    third = sum - first - second;

    std::cout << "Auf den dritten Kandidaten sind somit " << third << " Stimmen entfallen." << std::endl;
    std::cout << "Kandidat 1 erhielt " << first/sum * 100 << "% der Stimmen." << std::endl;
    std::cout << "Kandidat 2 erhielt " << second/sum * 100 << "% der Stimmen." << std::endl;
    std::cout << "Kandidat 3 erhielt " << third/sum * 100 << "% der Stimmen." << std::endl;


    return 0;
}