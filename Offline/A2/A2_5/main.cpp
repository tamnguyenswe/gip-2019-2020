#include "iostream"
#include "cctype"

int main() {
    char c;
    std::cout << "Bitte geben Sie den Kleinbuchstaben ein: ? ";
    std::cin >> c;

    std::cout << "Der entsprechende Grossbuchstabe lautet: " << char(toupper(c)) << "\n";

    return 0;
}