#include "iostream"

int main() {
    double a, b, temp;

    std::cout << "Bitte geben Sie den ganzzahligen Wert der ersten Variable ein: ? ";
    std::cin >> a;
    std::cout << "Bitte geben Sie den ganzzahligen Wert der zweiten Variable ein: ? ";
    std::cin >> b;

    // temp = a;
    // a = b;
    // b = temp;

    std::cout << "Nach dem Tausch:" << std::endl;
    std::cout << "Wert der ersten Variable: " << b << std::endl;    
    std::cout << "Wert der zweiten Variable: " << a << std::endl;

    return 0;
}