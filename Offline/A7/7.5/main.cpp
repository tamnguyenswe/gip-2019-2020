#include <iostream>
#include <string>

struct Morse {
    char character;
    std::string code;
};

const Morse morse_data[] = {
    {'a', ".-"},
    {'b', "-..."},
    {'c', "-.-."},
    {'d', "-.."},
    {'e', "."},
    {'f', "..-."},
    {'g', "--."},
    {'h', "...."},
    {'i', ".."},
    {'j', ".---"},
    {'k', "-.-"},
    {'l', ".-.."},
    {'m', "--"},
    {'n', "-."},
    {'o', "---"},
    {'p', ".--."},
    {'q', "--.-"},
    {'r', ".-."},
    {'s', "..."},
    {'t', "-"},
    {'u', "..-"},
    {'v', "...-"},
    {'w', ".--"},
    {'x', "-..-"},
    {'y', "-.--"},
    {'z', "--.."},
    {'0', "-----"},
    {'1', ".----"},
    {'2', "..---"},
    {'3', "...--"},
    {'4', "....-"},
    {'5', "....."},
    {'6', "-...."},
    {'7', "--..."},
    {'8', "---.."},
    {'9', "----."},
    {' ', "///"},
    {'.', ".-.-.-"},
    {',', "--..--"},
    {':', "---..."},
    {'?', "..--.."},
    {'-', "-....-"},
    {'/', "-..-."},
    {'@', ".--.-."},
    {'=', "-...-"}
};

const int TABLE_SIZE =  45;

std::string to_morse(char c) {
    for (int i = 0; i < TABLE_SIZE; i++) {
        if (morse_data[i].character == c)
            return morse_data[i].code;
    }

    return "";
}

int main() {
    std::string str;
    std::cout << "Bitte Text eingeben (ggfs. mit Leerzeichen): ? ";
    std::getline(std::cin, str);

    for (char c : str) {
        std::cout << to_morse(tolower(c)) << "#";
    }

    std::cout << "\n";

    return 0;
}