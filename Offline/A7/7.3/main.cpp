#include <iostream>
#include <string>

struct word_frequency {
    std::string word;
    int frequency;
};

bool is_alphabet(char c) {
    return ((c >= 'a') && (c <= 'z')) ||
           ((c >= 'A') && (c <= 'Z'));
}

std::string get_word(int left, int right, std::string str) {
    std::string result  = "";

    for (int i = left; i <= right; i++) {
        result += str[i];
    }

    return result;
}

void count(std::string this_word, word_frequency wf[], int& size) {
    for (int i = 0; i < size; i++) {
        if (this_word == wf[i].word) {
            wf[i].frequency++;
            return;
        }
    }

    // end of array, not found this word
    wf[size].frequency = 1;
    wf[size].word = this_word;

    size++;
}

int main() {
    std::string strings[4];
    for (std::string & str : strings) {
        std::cout << "Eingabezeile = ? ";
        std::getline(std::cin, str);
    }
//    strings[0] = "Dies ist die erste Zeile des Texts.";
//    strings[1] = "Und dies, ist, noch          eine Zeile.";
//    strings[2] = ".Und noch eine weitere Zeile, im Text.";
//    strings[3] = "";

    word_frequency result[1000];
    int word_counter = 0;
    for (auto & str : strings) {
        for (int i = 0; i < str.length(); i++) {

            int left = (is_alphabet(str[i])) ? i : 0;

            while (is_alphabet(str[i])) {
                i++;
                if (!is_alphabet(str[i])) {
                    int right = i - 1;
                    std::string this_word = get_word(left, right, str);

                    count(this_word, result, word_counter);
                }
            }
        }
    }

    for (int i = 0; i < word_counter; i++) {
        std::cout << result[i].word << ": " << result[i].frequency << "\n";
    }

    return 0;
}