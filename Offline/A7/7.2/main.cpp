#include <iostream>

bool is_alphabet(char c) {
    return ((c >= 'a') && (c <= 'z')) ||
           ((c >= 'A') && (c <= 'Z'));
}

int main() {
    std::string strings[4];
    for (std::string & str : strings) {
        std::cout << "Eingabezeile = ? ";
        std::getline(std::cin, str);
    }

    for (auto & str : strings) {
        for (int i = 0; i < str.length(); i++) {
            while (is_alphabet(str[i])) {
                std::cout << str[i++];

                if (!is_alphabet(str[i])) {
                    std::cout << "\n";
                }
            }
        }
    }
    return 0;
}