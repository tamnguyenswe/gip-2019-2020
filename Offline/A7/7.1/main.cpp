#include <iostream>
#include <string>

int main() {
    std::string strings[4];
    unsigned long max_length = 0;
    for (auto & str : strings) {
        std::cout << "Textzeile = ? ";
        std::getline(std::cin, str);
        max_length = (max_length > str.length()) ? max_length : str.length();
        if (str.length() == 0) break;
    }

    for (auto & str : strings) {
        if (str == "") continue;

        if (str.length() < max_length) {
            unsigned long hashes_needed = max_length - str.length();
            for (unsigned long j = 0; j < hashes_needed; j ++) std::cout << "#";
        }

        std::cout << str << "\n";
    }

    return 0;
}