#include <iostream>

char * my_strconcat(const char * ptr1, const char * ptr2) {
    char * result = (char *) calloc(40, sizeof(char));

    int index;
    for (index = 0; *ptr1 != '\0'; index++) {
        result[index] = *ptr1;
        ptr1++;
    }

    for (; *ptr2 != '\0'; index++) {
        result[index] = *ptr2;
        ptr2++;
    }

    return result;
}

int main() {
    char str1[20], str2[20];
    std::string foo;

    std::cout << "Bitte ersten Text eingeben (ggfs. mit Leerzeichen): ? ";
    std::cin.getline(str1, 20);
//
    std::cout << "Bitte zweiten Text eingeben (ggfs. mit Leerzeichen): ? ";
    std::cin.getline(str2, 20);

    std::cout << "Ergebnis my_strconcat(): " << my_strconcat(str1, str2) << "\n";

    return 0;
}
