#include "iostream"
#include "string.h"

int main() {
    std::string str;

    std::cout << "Bitte einzeiligen Text eingeben (ohne Leerzeichen): ? ";
    std::cin >> str;

    for (int i = 0; i < str.size(); ++i) {
        std::cout << "Position: " << i << " Buchstabe: " << str[i] << "\n";
    }

    return 0;
}