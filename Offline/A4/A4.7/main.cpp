#include "iostream"
#include "string.h"

int main() {
    std::string str;
    char c;

    std::cout << "Bitte Text eingeben (ggfs. mit Leerzeichen): ? ";
    std::getline(std::cin, str);

    std::cout << "Bitte Buchstaben eingeben: ? ";
    std::cin >> c;

    bool is_found = false;
    int position = 0;
    
    for (int i = 0; i < str.length(); i++) {
        if (str[i] == c) {
            is_found = true;
            position = i;
        }
    }

    if (is_found)
        std::cout << "Der Buchstabe " << c << " kommt im Text vor, an Position " << position << ".\n";
    else
        std::cout << "Der Buchstabe " << c << " kommt nicht im Text vor.\n";

    return 0;
}