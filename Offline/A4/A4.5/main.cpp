#include "iostream"
#include "string.h"

int main() {
    std::string str;

    bool is_valid = false;

    while (!is_valid) {
        std::cout << "Text: ? ";
        std::getline(std::cin, str);

        is_valid = true;
        for (int i = 0; i < str.length(); i++) {
            if ((str[i] < 'a') || (str[i] > 'z')) {
                is_valid = false;
                break;
            }
        }
    }

    bool is_palindrom = true;
    for (int i = 0; i < str.length()/2; i++) {
        if (str[i] != str[str.length() - 1 - i]) {
            is_palindrom = false;
            break;
        }
    }

    if (is_palindrom) 
        std::cout << "Das eingegebene Wort ist ein Palindrom.\n";
    else
        std::cout << "Das eingegebene Wort ist KEIN Palindrom.\n";


    return 0;
}