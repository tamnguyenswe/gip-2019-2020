#include "iostream"
#include "string.h"

int main() {
    int height, width;

    std::cout << "Bitte geben Sie die Breite des Parallelogramms ein: ? ";
    std::cin >> width;

    std::cout << "Bitte geben Sie die Hoehe des Parallelogramms ein: ? ";
    std::cin >> height;

    for (int i = 0; i < width; i++) 
        std::cout << "*";

    std::cout << '\n';

    for (int i = 0; i < height - 2; i++) {
        for (int j = 0; j < i + 1; j++)
            std::cout << '.';

        std::cout << '*';

        for (int j = 0; j < width - 2; j++) {
            std::cout << '+';
        }
        std::cout << "*\n";
    }

    for (int j = 0; j < height - 1; j++)
        std::cout << '.';

    for (int i = 0; i < width; i++) 
        std::cout << "*";
    
    std::cout << "\n\n";

    return 0;
}