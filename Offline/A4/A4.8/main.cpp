#include "iostream"
#include "string.h"

int main() {
    std::string str, raw;
    char c;

    std::cout << "Bitte Text eingeben (ggfs. mit Leerzeichen): ? ";
    std::getline(std::cin, str);

    std::cout << "Bitte Buchstaben eingeben: ? ";
    std::getline(std::cin, raw);

    // get all including space
    c = raw[0];

    int counter = 0;
    
    for (int i = 0; i < str.length(); i++) {
        if (str[i] == c) {
            counter++;
        }
    }

    std::cout << "Der Buchstabe " << c << " kommt " << counter << " mal im Text vor.\n";

    return 0;
}