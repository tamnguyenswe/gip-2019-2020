#include "iostream"
#include "string.h"

int main() {
    std::string str;
    char old_char, new_char;

    std::cout << "Bitte Text eingeben (ggfs. mit Leerzeichen): ? ";
    std::getline(std::cin, str);

    int i = 0;
    while (i < str.length()) {
        char this_char = str[i];

        // if this char is lowercase
        if ((this_char >= 'a') && (this_char <= 'z')) {
            // duplicate it once
            str.insert(i, 1, this_char);
            i += 1;
        } else

        // if this char is uppercase
        if ((this_char >= 'A') && (this_char <= 'Z')) {
            // duplicate it twice
            str.insert(i, 2, this_char);
            i += 2;
        } else 

        // if this char is a digit
        if ((this_char >= '0') && (this_char <= '9')) 
            // convert it into '.'
            str[i] = '.';
        else 
            
        // if this char is a '!' or '?'
        if ((this_char == '!') || (this_char == '?')) {
            // remove it 
            str.erase(i, 1);
            continue;

        } else

        // if this char is a space
        if (this_char == ' ')
            // conver it to '_'
            str[i] = '_';

        i++;
    }

    std::cout << "Der Text nach der Umwandlung: " << str << "\n";

    return 0;
}