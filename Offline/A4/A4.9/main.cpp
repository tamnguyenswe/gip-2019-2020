#include "iostream"
#include "string.h"

int main() {
    std::string str;
    char old_char, new_char;

    std::cout << "Bitte Text eingeben (ggfs. mit Leerzeichen): ? ";
    std::getline(std::cin, str);

    std::cout << "Bitte den zu ersetzenden Buchstaben eingeben: ? ";
    std::cin >> old_char;

    std::cout << "Bitte den Ersatz-Buchstaben eingeben: ? ";
    std::cin >> new_char;

    int counter = 0;
    
    for (int i = 0; i < str.length(); i++) {
        str[i] = (str[i] == old_char) ? new_char : str[i];
    }

    std::cout << "Der Text nach der Ersetzung: "<< str << "\n";

    return 0;
}