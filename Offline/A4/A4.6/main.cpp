#include "iostream"
#include "string.h"

int main() {
    std::string str;

    std::cout << "Bitte Text eingeben (ggfs. mit Leerzeichen): ? ";
    std::getline(std::cin, str);

    std::cout << "Eingabetext: " << str << "\nErgebnis: ";
    for (int i = str.length() -1; i >= 0; i--) {
        std::cout << str[i];
    }

    std::cout << '\n';

    return 0;
}