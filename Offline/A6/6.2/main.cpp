#include <iostream>

int main() {
    int a, b;
    std::cout << "Bitte geben Sie die erste Zahl ein: ? ";
    std::cin >> a;

    std::cout << "Bitte geben Sie die zweite Zahl ein: ? ";
    std::cin >> b;

    std::cout << a << " + " << b << " = " << a + b << "\n";
    std::cout << a << " - " << b << " = " << a - b << "\n";
    return 0;
}