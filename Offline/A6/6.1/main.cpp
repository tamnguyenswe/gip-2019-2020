#include <iostream>
#include <string>

void spalte_ab_erstem(char zeichen, std::string eingabe, std::string & erster_teil, std:: string& zweiter_teil) {
    int border = eingabe.length();
    for (int i = 0; i < eingabe.length(); i++) {
        if (eingabe[i] == zeichen) {
            border = i;
            break;
        }
    }

    // print left half
    std::cout << "Der erste Teil der Zeichenkette lautet: ";
    for (int i = 0; i < border; i++) {
        erster_teil += eingabe[i];
    }
    std::cout << erster_teil << "\n";

    // print right half
    std::cout << "Der zweite Teil der Zeichenkette lautet: ";
    for (int i = border + 1; i < eingabe.length(); i++) {
        zweiter_teil += eingabe[i];
    }

    std::cout << zweiter_teil << "\n";
}

int main() {
    std::string str, left_half, right_half;
    char c;

    std::cout << "Bitte geben Sie die einzeilige Zeichenkette ein: ? ";
    std::getline(std::cin, str);

    std::cout << "Bitte geben Sie das Zeichen ein: ? ";
    std::cin >> c;

    left_half = "";
    right_half = "";

    spalte_ab_erstem(c, str, left_half, right_half);

    return 0;
}